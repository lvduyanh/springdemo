package springDemo.dataTest;

import java.util.ArrayList;

import springDemo.models.Dog;

public class DumbData {
	private static ArrayList<Dog> arr = null;
	private static final Object key = new Object();
	
	private DumbData() {}
	
	public static ArrayList<Dog> GetArray() {
		if (arr != null) {
			return arr;
		}
		ArrayList<Dog> local = arr;
		if (local == null) {
			synchronized (key) {
				local = arr;
				if (local == null) {
					arr = new ArrayList<Dog>();
					arr.add(new Dog("1a", "dogbi", 5, 3.2));
					arr.add(new Dog("1b", "lulu", 3, 3));
					arr.add(new Dog("2a", "tofu", 4, 3.6));
					arr.add(new Dog("2b", "lifa", 2, 2.5));
					arr.add(new Dog("3a", "ri-chan", 7, 6.5));
					arr.add(new Dog("3c", "mup", 6, 7));
				}
				return arr;
			}
		}
		return null;
	}
}
