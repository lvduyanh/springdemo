package springDemo.controllers;

import java.io.BufferedReader;
import java.util.ArrayList;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;

import com.fasterxml.jackson.databind.ObjectMapper;

import springDemo.dataTest.DumbData;
import springDemo.models.Dog;

@RestController
public class DogController {

	@GetMapping("dogs")
	public String getAll() {
		ArrayList<Dog> dogs = DumbData.GetArray();
//		for (Dog d : dogs) {
//			System.out.println(d.toString());
//		}
		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = "";
		try {
			
			jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(dogs);
			System.out.println(jsonInString);
//			String tmp = "{\"id\":\"1a\",\"name\":\"dogbi\",\"age\":5,\"weight\":3.2}";
//			Dog d00 = mapper.readValue(tmp, Dog.class);
//			System.out.println(d00.toString());
			
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "Result: " + jsonInString;
	}
	
	@RequestMapping(value = "dog", method = RequestMethod.POST)
	public ResponseEntity updateDog(HttpServletRequest request) {
		String jsonStr = "";
		try {
			BufferedReader br = request.getReader();
			String tmp;
			while ((tmp = br.readLine()) != null) {
				jsonStr += tmp;
			}
			System.out.println(jsonStr);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ResponseEntity.ok(HttpStatus.OK);
	}
	
}
